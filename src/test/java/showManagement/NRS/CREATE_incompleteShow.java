package showManagement.NRS;

import java.io.IOException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class CREATE_incompleteShow extends smBASE{
	/*
	     Description: Create an incomplete show.
	 */
	@Test
	public void incompleteShow() throws IOException{
		String name = "CREATE_incompleteShow";
		String showStatus = "CREATE_incompleteShow";
		String URL = readValue(showStatus,"URL");
		String user = readValue(showStatus,"USER ID");
		String pass = readValue(showStatus,"PASSWORD");
		String run = readValue("Scripts",name);
		String resultSheet = "CREATE_incompleteShow";
		String resultBook = System.getProperty("user.dir")+"\\RESULTS\\"+date()+"CREATE_incompleteShow.xlsx";
		resultCol=1;

		if (run.equals("No"))
		{
			System.out.println("Create Show Script marked as NO for execution");
		} 
		else 
		{
			copyExcel(System.getProperty("user.dir")+"\\SETUPS\\CREATE_incompleteShow-sampleResults.xlsx",resultBook);
			
			// Call function to Initialize browser
			int last = lastRow("Run");
			for (int runCount=1; runCount<=last; runCount++)
			{
				//int runCount = 1;
				String Exec = readExcel("Run",runCount,4);
				if (Exec.contains("Yes"))
				{
					String platform = readExcel("Run",runCount,0);
					String browser = readExcel("Run",runCount,1);
					String version = readExcel("Run",runCount,2);
					String screenResolution = readExcel("Run",runCount,3);

					String showName = "AT"+date()+"Incomplete";
					resultCol=resultCol+1;
					resultRow =0;
					result(resultBook,resultSheet, name+"_"+platform+"_"+browser+"_"+version+"_"+screenResolution);
					try{
						run(platform, browser, version, screenResolution, name);
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						result(resultBook,resultSheet,"FAIL  "+ExceptionUtils.getStackTrace(t));
					}


					//---Call function to Launch Site URL and Login to myNRS site with valid credentials
					try{				
						loginNRS(URL,user,pass);
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						result(resultBook,resultSheet,"FAIL  "+ExceptionUtils.getStackTrace(t));
					}

					//---Call function to Click on "SHOW MANAGEMENT TOOL" option on left panel of the page and goto newly open Window
					try{			
						gotoShowManagement(URL);
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						result(resultBook,resultSheet,"FAIL  "+ExceptionUtils.getStackTrace(t));
					}

					//---Clicking Create Show button
					try {
						createButton();
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						result(resultBook,resultSheet,"FAIL  "+ExceptionUtils.getStackTrace(t));
					}

					//---Entering Show Details 
					try {
						baseWait("visible", lw, sm.getProperty("SM_C1_typeopt_drpopt_xpath"), "xpath");
						showDetails(showName, showStatus);
						driver.findElement(By.xpath(sm.getProperty("SM_C1_saveExit_btn_xpath"))).click();
						baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						result(resultBook,resultSheet,"FAIL  "+ExceptionUtils.getStackTrace(t));
					}

					//---Verify show displayed on Home page
					try {
						showVerify(showName);
						result(resultBook,resultSheet,"Pass "+showName);
					}
					catch(Throwable t)
					{
						result(resultBook,resultSheet,"FAIL  "+ExceptionUtils.getStackTrace(t));
					}
					super.closeBrowsers();  
				}
			}
		}
	}
}
