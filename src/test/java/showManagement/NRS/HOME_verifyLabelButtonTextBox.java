package showManagement.NRS;

import java.io.IOException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HOME_verifyLabelButtonTextBox extends smBASE{

	/*
	Description: user should see all the Labels, Buttons and Test boxes in show management tool Home page.
	 */

	@Test
	public void verifyLabelButtonTextBox() throws IOException
	{
		// Initializing Variables
		String name = "SM_verifyLabelButtonTextBox";
		String URL = readValue("CREATE_expiredShow","URL");
		String user = readValue("CREATE_expiredShow","USER ID");
		String pass = readValue("CREATE_expiredShow","PASSWORD");
		String run = readValue("Scripts",name);
		String resultSheet = "verifyElements";
		String resultBook = System.getProperty("user.dir")+"\\RESULTS\\"+date()+"verifyLabelButtonTextBox.xlsx";
		resultCol=1;

		// Check script execution status
		if (run.equals("No"))
		{
			System.out.println("Verify Home page elements Script marked as NO for execution");
		}
		else
		{
			copyExcel(System.getProperty("user.dir")+"\\SETUPS\\verifyLabelButtonTextBox-sampleResults.xlsx",resultBook);

			// Call function to Initialize browser
			int last = lastRow("Run");
			for (int runCount=1; runCount<=last; runCount++)
			{
				String Exec = readExcel("Run",runCount,4);
				if (Exec.contains("Yes"))
				{
					String platform = readExcel("Run",runCount,0);
					String browser = readExcel("Run",runCount,1);
					String version = readExcel("Run",runCount,2);
					String screenResolution = readExcel("Run",runCount,3);
					run(platform, browser, version, screenResolution, name);
					resultCol=resultCol+1;
					resultRow =0;
					result(resultBook,resultSheet, name+"_"+platform+"_"+browser+"_"+version+"_"+screenResolution);

					// Call function to Launch Site URL and Login to myNRS site with valid credentials
					loginNRS(URL,user,pass);

					// Call function to Click on "SHOW MANAGEMENT TOOL" option on left panel of the page and goto newly open Window
					gotoShowManagement(URL);

					// Wait for Home page of Show Management page to load
					try
					{
						//baseWait("visible",lw,sm.getProperty("SM_Loader_xpath"),"xpath");
						baseWait("invisible",lw,sm.getProperty("SM_Loader_xpath"),"xpath");
						baseWait("visible",lw,sm.getProperty("SM_home_createNew_btn_xpath"),"xpath");  
						System.out.println("0: GOOD: Waiter loader wait required");
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("0: WARNING: Not loader wait required");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Home" Label in Home page of Show Management page
					try
					{
						baseWait("visible", lw, sm.getProperty("SM_home_Label_xpath"), "xpath");
						//new WebDriverWait(driver, lw).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(sm.getProperty("SM_home_Label_xpath"))));
						Assert.assertEquals("Home", driver.findElement(By.xpath(sm.getProperty("SM_home_Label_xpath"))).getText());
						System.out.println("4: PASS: 'Home' Label is visible to Admin User");
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("4: FAIL: 'Home' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}	

					// Verify the "LIVE" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("live",driver.findElement(By.xpath(sm.getProperty("SM_live_lbl_xpath"))).getText().toLowerCase());//create method 3rd para true/false(only for MS edge by puting in global variable t/f)
						System.out.println("live lable is present in smallcase in HTML");
						Assert.assertEquals(driver.findElement(By.xpath(sm.getProperty("SM_live_lbl_xpath"))).getCssValue("text-transform"),"uppercase");
						System.out.println("Text transformation to uppercase has been applied in CSS. Hence final lable displayed is \"LIVE\"");
						System.out.println("5: PASS: 'LIVE' Label is visible to Admin User");				
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("5: FAIL: 'LIVE' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
						System.out.println(ExceptionUtils.getStackTrace(t));
					}	

					// Verify the "INCOMPLETE" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("incomplete", driver.findElement(By.xpath(sm.getProperty("SM_incomplete_lbl_xpath"))).getText().toLowerCase());
						System.out.println("incomplete lable is present in smallcase in HTML");
						Assert.assertEquals(driver.findElement(By.xpath(sm.getProperty("SM_incomplete_lbl_xpath"))).getCssValue("text-transform"),"uppercase");
						System.out.println("Text transformation to uppercase has been applied in CSS. Hence final lable displayed is \"INCOMPLETE\"");
						System.out.println("6: PASS: 'INCOMPLETE' Label is visible to Admin User");			
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("6: FAIL: 'INCOMPLETE' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}	

					// Verify the "DISABLED" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("disabled", driver.findElement(By.xpath(sm.getProperty("SM_disabled_lbl_xpath"))).getText().toLowerCase());
						System.out.println("disabled lable is present in smallcase in HTML");
						Assert.assertEquals(driver.findElement(By.xpath(sm.getProperty("SM_disabled_lbl_xpath"))).getCssValue("text-transform"),"uppercase");
						System.out.println("Text transformation to uppercase has been applied in CSS. Hence final lable displayed is \"DISABLED\"");
						System.out.println("7: PASS: 'DISABLED' Label is visible to Admin User");					
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("7: FAIL: 'DISABLED' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}	

					// Verify the "EXPIRING SOON" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("expiring soon", driver.findElement(By.xpath(sm.getProperty("SM_expiringSoon_lbl_xpath"))).getText().toLowerCase());
						System.out.println("expiring soon lable is present in smallcase in HTML");
						Assert.assertEquals(driver.findElement(By.xpath(sm.getProperty("SM_expiringSoon_lbl_xpath"))).getCssValue("text-transform"),"uppercase");
						System.out.println("Text transformation to uppercase has been applied in CSS. Hence final lable displayed is \"EXPIRING SOON\"");
						System.out.println("8: PASS: 'EXPIRING SOON' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("8: FAIL: 'EXPIRING SOON' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "EXPIRED" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("expired", driver.findElement(By.xpath(sm.getProperty("SM_expire_lbl_xpath"))).getText().toLowerCase());
						System.out.println("expired lable is present in smallcase in HTML");
						Assert.assertEquals(driver.findElement(By.xpath(sm.getProperty("SM_expire_lbl_xpath"))).getCssValue("text-transform"),"uppercase");
						System.out.println("Text transformation to uppercase has been applied in CSS. Hence final lable displayed is \"EXPIRED\"");
						System.out.println("9: PASS: 'EXPIRED' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("9: FAIL: 'EXPIRED' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Previously Created Shows (307)" Label in Home page of Show Management page
					try
					{		
						String string = driver.findElement(By.xpath(sm.getProperty("SM_perivouslyCreatedShow_lbl_xpath"))).getText();
						String[] parts = string.split(" ");
						String part1 = parts[0];
						String part2 = parts[1];
						String part3 = parts[2];
						String all = part1+" "+part2+" "+part3;
						System.out.println(all);
						Assert.assertEquals("Previously Created Shows", all);
						System.out.println("10: PASS: 'Previously Created Shows' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("10: FAIL: 'Previously Created Shows' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Sort By:" Label in Home page of Show Management page
					try
					{
						if(browser.equalsIgnoreCase("edge"))
						{
							Assert.assertEquals("Sort By: ", driver.findElement(By.xpath(sm.getProperty("SM_sortby_lbl_xpath"))).getText());
						}
						else
						{
							Assert.assertEquals("Sort By:", driver.findElement(By.xpath(sm.getProperty("SM_sortby_lbl_xpath"))).getText());
						}

						System.out.println("11: PASS: 'Sort By:' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("11: FAIL: 'Sort By:' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Filter By: " Label in Home page of Show Management page
					try
					{
						if(browser.equalsIgnoreCase("edge"))
						{
							Assert.assertEquals("Filter By: ", driver.findElement(By.xpath(sm.getProperty("SM_filterby_lbl_xapth"))).getText());
						}
						else
						{
							Assert.assertEquals("Filter By:", driver.findElement(By.xpath(sm.getProperty("SM_filterby_lbl_xapth"))).getText());
						}

						System.out.println("12: PASS: 'Filter By:' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("12: FAIL: 'Filter By:' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Show Name" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("Show Name", driver.findElement(By.xpath(sm.getProperty("SM_showName_lbl_xpath"))).getText());
						System.out.println("13: PASS: 'Show Name' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("13: FAIL: 'Show Name' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Start Date" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("Start Date", driver.findElement(By.xpath(sm.getProperty("SM_startDate_lbl_xpath"))).getText());
						System.out.println("14: PASS: 'Start Date' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("14: FAIL: 'Start Date' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "End Date" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("End Date", driver.findElement(By.xpath(sm.getProperty("SM_endDate_lbl_xpath"))).getText());
						System.out.println("15: PASS: 'End Date' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("15: FAIL: 'End Date' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Show Status" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("Show Status", driver.findElement(By.xpath(sm.getProperty("SM_showStatus_lbl_xpath"))).getText());
						System.out.println("16: PASS: 'Show Status' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("16: FAIL: 'Show Status' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Entry Code" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("Entry Code", driver.findElement(By.xpath(sm.getProperty("SM_entryCode_lbl_xpath"))).getText());
						System.out.println("17: PASS: 'Entry Code' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("17: FAIL: 'Entry Code' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}

					// Verify the "Actions" Label in Home page of Show Management page
					try
					{
						Assert.assertEquals("Actions", driver.findElement(By.xpath(sm.getProperty("SM_actions_lbl_xpath"))).getText());
						System.out.println("18:LAST PASS: 'Actions' Label is visible to Admin User");						
						result(resultBook,resultSheet,"Pass");
					}
					catch(Throwable t)
					{
						System.out.println("18:LAST FAIL: 'Actions' Label is not visible to Admin User. Test case fail");
						result(resultBook,resultSheet,"Fail "+ExceptionUtils.getStackTrace(t));
					}
					super.closeBrowsers();
				}
				else
				{
					System.out.println("...............Run marks as NO..............."+name);
				}
			}
		}
	}
}