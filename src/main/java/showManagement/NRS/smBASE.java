package showManagement.NRS;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

public class smBASE extends BASE
{

	public Properties sm = null;

	//Launch SM OR
	@BeforeTest
	public void smOR() throws IOException
	{
		try
		{
			File src1=new File(System.getProperty("user.dir")+"\\OBJECT REPOSITORIES\\showManagementOR.properties");
			FileInputStream fis1=new FileInputStream(src1);
			sm =new Properties();
			sm.load(fis1);
		}
		catch(Throwable t)
		{
			System.out.println("1: FAIL: not able to launch sm OR successfully.");
//			System.exit(0);
		}
	}

	// Launch Site URL and Login to myNRS site with valid credentials
	public void loginNRS(String URL, String user, String pass) throws IOException
	{
		try
		{
			driver.get(URL);	
			driver.findElement(By.id(sm.getProperty("nrs_login_Username_txt_id"))).sendKeys(user);
			driver.findElement(By.id(sm.getProperty("nrs_login_Password_txt_id"))).sendKeys(pass);
			driver.findElement(By.id(sm.getProperty("nrs_login_login_btn_id"))).click();
//			baseWait("clickable",lw,sm.getProperty("nrs_home_ShowManagementTool_lnk_xpath"),"xpath");
			System.out.println("2: PASS: URL:"+URL+" User ID: "+user+" is able to login successfully.");
		}
		catch(Throwable t)
		{
			System.out.println("2: FAIL: URL:"+URL+" User ID: "+user+" User Pass: "+pass+" is not able to login successfully.");
		}		
	}

	// Goto "SHOW MANAGEMENT TOOL" 
	public void gotoShowManagement(String URL) throws IOException
	{	
		try
		{
//			driver.navigate().to(URL);
//			String[] parts = URL.split("nrs");
//			String part1 = parts[0];
//			String smURL = part1+"nrsadmin/#/shows/home";
			Thread.sleep(1000);
			baseWait("clickable",lw,sm.getProperty("nrs_home_ShowManagementTool_lnk_xpath"),"xpath");
			Thread.sleep(5000);
			driver.findElement(By.xpath(sm.getProperty("nrs_home_ShowManagementTool_lnk_xpath"))).click();
			Thread.sleep(3000);
			
			Set<String> win_list =driver.getWindowHandles();
			Iterator<String>itr=win_list.iterator();
			String primary=itr.next();
			String secondary=itr.next();
			driver.switchTo().window(secondary).manage().window();
			driver.manage().window().maximize();
			System.out.println("3.2: PASS: User routes to secondary window for Show Management Tool.");
			
//			do {
//				driver.navigate().to(smURL);
//				driver.navigate().to("http://test2.netroadshow.com/nrsadmin/#/shows/home");
				Thread.sleep(300);
//			} while (!driver.getCurrentUrl().contains("shows/home"));
			//driver.get(smURL);
			System.out.println("3.1: PASS: User routed to Show Management Home Page.");
		}
		catch(Throwable t)
		{
			System.out.println("3.1 FAIL: User unable to route to myNRS Home Page or 'SHOW MANAGEMENT TOOL' option not visible.");
		}
	}

	// loader wait
	public void loadWait1()
	{
		try
		{
			new WebDriverWait(driver, mw).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(sm.getProperty("SM_Loader_xpath"))));
			new WebDriverWait(driver, mw).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(sm.getProperty("SM_Loader_xpath"))));
		}
		catch(Throwable t)
		{
			System.out.println("load wait not required.");
		}
	}

	// Click "CREATE NEW" show button
	public void createButton()
	{
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible",lw,sm.getProperty("SM_home_createNew_btn_xpath"),"xpath");                                                 
		driver.findElement(By.xpath(sm.getProperty("SM_home_createNew_btn_xpath"))).click();                                                 
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");                                              
		baseWait("visible",lw,sm.getProperty("SM_C1_title_txt_id"),"id");
	}
	
	public void newButton()
	{
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible",lw,sm.getProperty("SM_home_newShow_btn_lnk"),"link");                                                 
		driver.findElement(By.linkText(sm.getProperty("SM_home_newShow_btn_lnk"))).click();                                                 
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");                                              
		baseWait("visible",lw,sm.getProperty("SM_C1_title_txt_id"),"id");
	}

	// Enter "Show Details" page details, ShowManagement_Homepage->CreateNew->ShowDetails
	public void showDetails(String ShowName, String Status) throws InterruptedException, IOException
	{
		String startDate = readValue(Status,"Start Date (YY)");
		//		String startTime = readValue(Status,"Start Time");
		String endDate = readValue(Status,"End Date (YY)");
		//		String endTime = readValue(Status,"End Time");
		driver.findElement(By.id(sm.getProperty("SM_C1_title_txt_id"))).sendKeys(ShowName);
		driver.findElement(By.xpath(sm.getProperty("SM_C1_typeopt_drpopt_xpath"))).click();
		driver.findElement(By.xpath(sm.getProperty("SM_C1_sectoropt_drpopt_xpath"))).click();
		try {
			driver.findElement(By.xpath(sm.getProperty("SM_C1_BG_drop_xpath"))).isDisplayed();
			driver.findElement(By.xpath(sm.getProperty("SM_C1_BG_drop_xpath"))).click();
			driver.findElement(By.xpath(sm.getProperty("SM_C1_BGopt_drpopt_xpath"))).click();
		} catch (Exception e) {
			System.out.println("This underwriter has only one show group");
		}
		driver.findElement(By.id(sm.getProperty("SM_C1_title_txt_id"))).click();
		driver.findElement(By.id(sm.getProperty("SM_C1_startDate_txt_id"))).click();
		Thread.sleep(2000);
		driver.findElement(By.id(sm.getProperty("SM_C1_startDate_txt_id"))).sendKeys("1");
		Thread.sleep(3000);
		driver.findElement(By.id(sm.getProperty("SM_C1_startDate_txt_id"))).sendKeys(Keys.BACK_SPACE);
		driver.findElement(By.id(sm.getProperty("SM_C1_startDate_txt_id"))).sendKeys(startDate);
		Thread.sleep(1000);
		driver.findElement(By.id(sm.getProperty("SM_C1_title_txt_id"))).click();
		driver.findElement(By.id(sm.getProperty("SM_C1_endDate_txt_id"))).click();
		Thread.sleep(2000);
		driver.findElement(By.id(sm.getProperty("SM_C1_endDate_txt_id"))).sendKeys("1");
		Thread.sleep(3000);
		driver.findElement(By.id(sm.getProperty("SM_C1_endDate_txt_id"))).sendKeys(Keys.BACK_SPACE);
		driver.findElement(By.id(sm.getProperty("SM_C1_endDate_txt_id"))).sendKeys(endDate);
	}

	// Enter "Slides" page details, ShowManagement_Homepage->CreateNew->Slides
	public void slides() throws InterruptedException, IOException
	{
		String filePath = System.getProperty("user.dir")+"\\autoPDF.pdf";
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C2_SlidesLeftNav_lnk_xpath"))).click();
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C2_noSlideIcon_grd_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C2_selectFile_btn_xpath"), "xpath");
		WebElement upload = driver.findElement(By.xpath(sm.getProperty("SM_C2_selectFile_pth_xpath")));
		upload.sendKeys(filePath);
		baseWait("invisible", lw, sm.getProperty("SM_C2_uploadDisabled_btn_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C2_upload_btn_xpath"))).click();
		baseWait("visible", lw, sm.getProperty("SM_C2_gridArea_grd_xpath"), "xpath");
		// Validate the data appeared under the Grid
		Assert.assertEquals("1",driver.findElement(By.xpath(sm.getProperty("SM_C2_slideno_lbl_xpath"))).getText());
		driver.findElement(By.xpath(sm.getProperty("SM_C1_submit_btn_xpath"))).click();
		System.out.println("4.2: PASS: 'Slides' page successfully submitted.");
	}

	// Enter "Documentation" page details, ShowManagement_Homepage->CreateNew->Documentation
	public void documentation(String status) throws InterruptedException, IOException
	{
		String documentType = readValue(status,"Document Type");
		String documentName = readValue(status,"Document Name");
		String filePath = System.getProperty("user.dir")+"\\autoPDF.pdf";
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C3_documentsLeftNav_lnk_xpath"))).click();
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C3_documentation_noDcoumentIcon_xpath"), "xpath");
		Select docType = new Select(driver.findElement(By.id(sm.getProperty("SM_C3_documentType_drp_id"))));
		docType.selectByVisibleText(documentType);
		driver.findElement(By.id(sm.getProperty("SM_C3_documentName_txt_id"))).sendKeys(documentName);
		WebElement upload = driver.findElement(By.xpath(sm.getProperty("SM_C3_selectFile_pth_xpath")));
		upload.sendKeys(filePath);
		driver.findElement(By.xpath(sm.getProperty("SM_C3_upload_btn_xpath"))).click();
		// Validate the data under Grid
		baseWait("visible", lw, sm.getProperty("SM_C2_gridArea_grd_xpath"), "xpath");
		Assert.assertEquals(documentType,
		driver.findElement(By.xpath(sm.getProperty("SM_C3_documentType_lbl_xpath"))).getText());
		driver.findElement(By.xpath(sm.getProperty("SM_C3_save_btn_xpath"))).click();
		System.out.println("4.3: PASS: 'Documentation' page successfully submitted.");
	}

	/*// Enter "Disclaimer" page details, ShowManagement_Homepage->CreateNew->Disclaimer
	public void disclaimer() throws InterruptedException, IOException
	{
//		String disclaimerName = readValue("SM Data","Disclaimer Name");
//		String disclaimerText = readValue("SM Data","Disclaimer Text");
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerLeftNav_lnk_xpath"))).click();
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C2_noSlideIcon_grd_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerList_xpath"))).click();
		driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerListOpt_xpath"))).click();
		//driver.findElement(By.id(sm.getProperty("SM_C4_disclaimerName_txt_id"))).sendKeys(disclaimerName);
		//driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerText_txt_xpath"))).sendKeys(disclaimerText);
		if(driver.findElement(By.xpath("//div[@class='fr-element fr-view']/p[1]")).getText()!=null)
			{System.out.println("Disclaimer entered");}

		baseWait("invisible", lw, sm.getProperty("SM_C2_uploadDisabled_btn_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C4_add_btn_xpath"))).click();
//		Assert.assertEquals(disclaimerName,driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerName_lbl_xpath"))).getText());
		driver.findElement(By.xpath(sm.getProperty("SM_C4_submit_btn_xpath"))).click();
		System.out.println("4.4: PASS: 'Disclaimer' page successfully submitted.");
	}
	 */

	// Enter "Disclaimer" page details, ShowManagement_Homepage->CreateNew->Disclaimer
	public void disclaimer(String status) throws InterruptedException, IOException
	{
		String disclaimerName = readValue(status,"Disclaimer Name");
		String disclaimerText = readValue(status,"Disclaimer Text");
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerLeftNav_lnk_xpath"))).click();
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C2_noSlideIcon_grd_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerList_xpath"))).click();
		driver.findElement(By.xpath(sm.getProperty("SM_C4_customDisclaimer_lnk_xpath"))).click();
		//driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerListOpt_xpath"))).click();
		driver.findElement(By.id(sm.getProperty("SM_C4_disclaimerName_txt_id"))).sendKeys(disclaimerName);
		driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerText_txt_xpath"))).sendKeys(disclaimerText);
		//    driver.findElement(By.xpath("//textarea[@name='disclaimerText']")).sendKeys(disclaimerText);
		//if(driver.findElement(By.xpath("//div[@class='fr-element fr-view']/p[1]")).getText()!=null)
		//{System.out.println("Disclaimer entered");}
		Thread.sleep(2000);
		baseWait("invisible", lw, sm.getProperty("SM_C2_uploadDisabled_btn_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C4_add_btn_xpath"))).click();
		//  Assert.assertEquals(disclaimerName,driver.findElement(By.xpath(sm.getProperty("SM_C4_disclaimerName_lbl_xpath"))).getText());
		driver.findElement(By.xpath(sm.getProperty("SM_C4_submit_btn_xpath"))).click();
		System.out.println("4.4: PASS: 'Disclaimer' page successfully submitted.");
	}

	// Enter "Entry Codes" page details, ShowManagement_Homepage->CreateNew->Entry Codes
	public void entryCodes() throws InterruptedException
	{
		String code = null;
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C5_entryCodeLeftNav_lnk_xpath"))).click();
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C2_noSlideIcon_grd_xpath"), "xpath");
		driver.findElement(By.linkText(sm.getProperty("SM_C5_5entryCodes_lnk_lnk"))).click();
		driver.findElement(By.xpath(sm.getProperty("SM_C5_generateRandom_btn_xpath"))).click();
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		code = driver.findElement(By.id("EntryCode")).getAttribute("value");
		driver.findElement(By.xpath(sm.getProperty("SM_C5_add_btn_xpath"))).click();
		baseWait("visible", lw, sm.getProperty("SM_C2_gridArea_grd_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C5_randomEntryCode_lbl_xpath"), "xpath");
		//		Assert.assertEquals(code,driver.findElement(By.xpath(sm.getProperty("SM_C5_randomEntryCode_lbl_xpath"))).getText());
		driver.findElement(By.xpath(sm.getProperty("SM_C5_submit_btn_xpath"))).click();
		System.out.println("4.5: PASS: 'Entry Codes' page successfully submitted with Entry Code generated randomly: "+ code);
	}

	// Verify Billing page details, ShowManagement_Homepage->CreateNew->Billing
	public void billing(String showStatus) throws InterruptedException, IOException
	{
		if (readValue(showStatus, "User Role").equalsIgnoreCase("underwriter"))
		{
			baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
			driver.findElement(By.xpath(sm.getProperty("SM_C6_billingLeftNav_lnk_xpath"))).click();
			baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
			driver.findElement(By.id("FirstName")).sendKeys("Test");
			driver.findElement(By.id("LastName")).sendKeys("User");
			driver.findElement(By.id("Company")).sendKeys("Test Company");
			driver.findElement(By.id("EmailAddress")).sendKeys("abc@test.com");
			driver.findElement(By.id("PhoneNumber")).sendKeys("123456789");					
			Select country = new Select(driver.findElement(By.id("type")));
			country.selectByVisibleText("India");					
			baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");					
			driver.findElement(By.id("StreetAddress")).sendKeys("Rolta Tower A");
			driver.findElement(By.id("City")).sendKeys("xyz");					
			Select state = new Select(driver.findElement(By.id("State")));
			state.selectByVisibleText("Delhi");					
			driver.findElement(By.id("Zip")).sendKeys("12345");										
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(sm.getProperty("SM_C6_InvoiceNo_btn_xpath"))));
			driver.findElement(By.xpath(sm.getProperty("SM_C6_InvoiceNo_btn_xpath"))).click();
			driver.findElement(By.xpath(sm.getProperty("SM_C6_Add_btn_xpath"))).click();					
			baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");					
			StringBuilder amount =new StringBuilder(driver.findElement(By.xpath(sm.getProperty("SM_C6_remainingBalance_txt_xpath"))).getText());
			amount.deleteCharAt(0);
			String finalAmount = amount.toString();				
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(sm.getProperty("SM_C6_newBillerBlock_lbl_xpath"))));
			driver.findElement(By.xpath(sm.getProperty("SM_C6_AmountToBill_box_xpath"))).sendKeys(finalAmount);					
			driver.findElement(By.xpath(sm.getProperty("SM_C6_newBillerName_lbl_xpath"))).click();				
			driver.findElement(By.xpath(sm.getProperty("SM_C5_submit_btn_xpath"))).click();	
		}
		else 
		{
			System.out.println("Logged in user is not underwriter");
		}
	}
	
	// *** Verify "Review" page details, ShowManagement_Homepage->CreateNew->Review
	public void review(String showName, String showStatus) throws InterruptedException, IOException
	{
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_C7_reviewLeftNav_lnk_xpath"))).click();
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		if (readValue(showStatus, "User Role").equalsIgnoreCase("underwriter"))
		{
			System.out.println("Its an underwriter");
		}
		else
		{
			driver.findElement(By.xpath(sm.getProperty("SM_C7_review_lnk_xpath"))).click();
		}
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_C7_showTitle_lbl_xpath"), "xpath");
		Assert.assertEquals(showName, driver.findElement(By.xpath(sm.getProperty("SM_C7_showName_lbl_xpath"))).getText());
		driver.findElement(By.xpath(sm.getProperty("SM_C7_makeShowLive_btn_xpath"))).click();
		if (readValue(showStatus, "User Role").equalsIgnoreCase("underwriter"))
		{
			baseWait("visible", lw, sm.getProperty("SM_C7_confirmAgree_btn_xpath"), "xpath");
			driver.findElement(By.xpath(sm.getProperty("SM_C7_confirmAgree_btn_xpath"))).click();
		}
		else
		{
			baseWait("visible", lw, sm.getProperty("SM_C7_confirmYes_btn_xpath"), "xpath");
			driver.findElement(By.xpath(sm.getProperty("SM_C7_confirmYes_btn_xpath"))).click();
		}				
		System.out.println("4.6: PASS: Show created successfully.");
	}

	// Search and Verify the Show Name
	public void showVerify(String showName) throws InterruptedException
	{
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		baseWait("visible", lw, sm.getProperty("SM_home_createNew_btn_xpath"), "xpath");
		driver.findElement(By.xpath(sm.getProperty("SM_searchShow_txt_xpath"))).sendKeys(showName);
		baseWait("invisible", lw, sm.getProperty("SM_Loader_xpath"), "xpath");
		String searchshownamexpath = "//a[contains(@title,'"+showName+"')]";
		Assert.assertEquals(showName, driver.findElement(By.xpath(searchshownamexpath)).getText());
//		Assert.assertEquals(showName, driver.findElement(By.xpath(sm.getProperty("SM_firstRow_lnk_xpath"))).getText());
		System.out.println("4.00: PASS: Show verified successfully.");
	}	

	// unsaved change error handling
	public void unSaveChanges(){
		try {
			baseWait("visible", lw,sm.getProperty("SM_unSave_Save_btn_xpath"), "xpath");		
			driver.findElement(By.xpath(sm.getProperty("SM_unSave_Save_btn_xpath"))).click();
		} catch (Exception e) {
			System.out.println("No unsaved changes");
		}
	} 

}
