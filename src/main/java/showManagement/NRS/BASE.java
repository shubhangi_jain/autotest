package showManagement.NRS;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.remote.LocalFileDetector;

public class BASE {

	public Properties cOR=null;
	public RemoteWebDriver driver=null;
//	public WebDriver driver=null;
	public DesiredCapabilities caps = null;
	public Integer sw = 5;
	public Integer mw = 10;
	public Integer lw = 30;
	public  int wc = 0;
	public String workbookTestData = System.getProperty("user.dir")+"\\testData.xlsx";
	public  int resultRow = 0;
	public  int resultCol = 1;

	// common OR
	@BeforeTest
	public void cOR() throws IOException {
		try {
			File src=new File(System.getProperty("user.dir")+"\\OBJECT REPOSITORIES\\commonOR.properties");
			FileInputStream fis=new FileInputStream(src);
			cOR =new Properties();
			cOR.load(fis);
		}catch(Throwable t){
			System.out.println("1: FAIL: not able to launch base OR successfully.");
			System.exit(0);
		}
	}

	
	//Launch configuration
	public void run(String platform, String browser, String version, String screenResolution, String name) throws IOException 
	{
		System.out.println(platform);
		try{
			if(platform.equals("Local PC"))
			{
				switch (browser) 
				{
				case "Chrome":
					System.setProperty("webdriver.chrome.driver", "C:\\Users\\harshitahluwalia\\Desktop\\HA\\Eclipse\\NRS\\chromedriver.exe");
					driver = new ChromeDriver();
					break;
				case "IE":
					System.setProperty("webdriver.ie.driver", "C:\\Users\\harshitahluwalia\\Desktop\\HA\\Eclipse\\NRS\\IEDriverServer.exe");
					driver = new InternetExplorerDriver();
					break;
				case "FireFox":
					System.setProperty("webdriver.gecko.driver", "C:\\Users\\harshitahluwalia\\Desktop\\HA\\Eclipse\\NRS\\geckodriver.exe");
					driver = new FirefoxDriver();
					break;
				default:
					System.out.println("No driver for Local");
					break;
				}        
			}
			else if(platform.equals("Local Android"))
			{
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("deviceName", "Auto6");
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("browserName", "Chrome");
				driver = new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"),capabilities);
			}
			else
			{
				String USERNAME = readValue("Sauce Labs","USERNAME");
				String ACCESS_KEY = readValue("Sauce Labs","ACCESS_KEY");
				String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
				if(platform.equals("Android")){
					 caps = DesiredCapabilities.android();
					  caps.setCapability("appiumVersion", "1.6.4");
					  caps.setCapability("deviceName",screenResolution);
					  caps.setCapability("deviceOrientation", "portrait");
					  caps.setCapability("platformVersion",version);
					  caps.setCapability("platformName", platform);
					  caps.setCapability("browserName", browser);
				}else{
				
					switch(browser)
					{
					//all chrome
					case "Chrome":
						caps = DesiredCapabilities.chrome();
						break;
						//all IE
					case "IE":
						caps = DesiredCapabilities.internetExplorer();
						break;
						// all Edge
					case "Edge":
//						caps = DesiredCapabilities.edge();
						break;
						// all ff
					case "FireFox":
						caps = DesiredCapabilities.firefox();
						break;
					case "Safari":
						caps = DesiredCapabilities.safari();
						break;
					}
					caps.setCapability("platform", platform);
					caps.setCapability("version", version);
//					caps.setCapability("name", name+"_"+platform+"_"+browser+"_"+version+"_"+screenResolution);
					caps.setCapability("screenResolution", screenResolution);
				}
				caps.setCapability("name", name+"_"+platform+"_"+browser+"_"+version+"_"+screenResolution);
				driver = new RemoteWebDriver(new URL(URL), caps);
				//			driver.setFileDetector(new LocalFileDetector());
				((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
			}
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(mw, TimeUnit.SECONDS);
			System.out.println("0: PASS: launch run successfully."+name+"_"+platform+"_"+browser+"_"+version+"_"+screenResolution);
		}catch(Throwable t){
			System.out.println("0: FAIL: not able to launch run successfully.");
		}
	}

	
	//*** Return Last row from Excel
	public int lastRow(String sheet) throws IOException {
		XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(workbookTestData));
		XSSFSheet myExcelSheet = myExcelBook.getSheet(sheet);
		int lastR = myExcelSheet.getLastRowNum();		
		myExcelBook.close();
		return (lastR);
	}

	//*** Read Variable data from Excel
	public String readValue(String sheet, String value) throws IOException {
		String ans=null;
		XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(workbookTestData));
		XSSFSheet myExcelSheet = myExcelBook.getSheet(sheet);
		int lastR = myExcelSheet.getLastRowNum();
		for (int lr=0; lr<=lastR;lr++){
			String data=myExcelSheet.getRow(lr).getCell(0).getStringCellValue();
			if(data.equals(value)){
				ans=myExcelSheet.getRow(lr).getCell(1).getStringCellValue();
			}
		}	
		myExcelBook.close();
		return (ans);
	}

	//*** Read data from Excel
	public String readExcel(String sheet, Integer row, Integer col) throws IOException {
		XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(workbookTestData));
		XSSFSheet myExcelSheet = myExcelBook.getSheet(sheet);
		String data=myExcelSheet.getRow(row).getCell(col).getStringCellValue();
		myExcelBook.close();
		return (data);
	}

	//*** Write data to Excel
	public void writeExcel(String workbook, String sheet, int row, int col, String data) throws FileNotFoundException, IOException {	
		try{
			XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(workbook));
			XSSFSheet myExcelSheet = myExcelBook.getSheet(sheet);
			myExcelSheet.getRow(row).createCell(col).setCellValue(data);
			myExcelBook.write(new FileOutputStream(new File(workbook)));
			myExcelBook.close();
		}catch(Throwable t){
			System.out.println("0: FAIL: not able to launch writeExcel successfully.");
		}
	}

	//*** Copy Excel file
	public void copyExcel(String srcBook, String dstbook) throws FileNotFoundException, IOException {	
		try{
			XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(srcBook));
			myExcelBook.write(new FileOutputStream(new File(dstbook)));
			myExcelBook.close();
		}catch(Throwable t){
			System.out.println("0: FAIL: not able to launch copyExcel successfully.");
		}
	}

	//*** Result data to Excel
	public void result(String resultBook, String sheet, String data) throws FileNotFoundException, IOException {	
		try{
			XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(resultBook));
			XSSFSheet myExcelSheet = myExcelBook.getSheet(sheet);
			myExcelSheet.getRow(resultRow).createCell(resultCol).setCellValue(data);
			myExcelBook.write(new FileOutputStream(new File(resultBook)));
			myExcelBook.close();
			resultRow = resultRow+1;
		}catch(Throwable t){
			System.out.println("0: FAIL: not able to launch result successfully.");
		}
	}

	public String generateString(int length) {
		//*** Function to create random string 
		String allowedChars="abcdefghijklmnopqrstuvwxyz";
		String text="";
		String temp=RandomStringUtils.random(length,allowedChars);
		text="AutoShow"+temp.substring(0,temp.length());
		return text;
	}

	// base wait for different type of waits
	public void baseWait(String waitType, int time, String locator, String locatorType)
	{       
		wc=wc+1;
		try
		{
			switch (waitType) 
			{
			// wait till the element is visible
			case "visible":
				new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOfElementLocated(elementLocator(locator, locatorType)));
				break;
				// wait till the element is invisible
			case "invisible":
				new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementLocated(elementLocator(locator, locatorType)));  
				break;
				// wait till the element is click-able
			case "clickable":
				new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(elementLocator(locator, locatorType)));
				break;
				// wait till the element is Present
			case "presence":
				new WebDriverWait(driver, time).until(ExpectedConditions.presenceOfElementLocated(elementLocator(locator, locatorType)));
				break;  
			default:
				System.out.println("Wait type mismatch:-- Element with wait type:"+waitType+wc);             
			}  
		}
		catch(Throwable t)
		{
			System.out.println("wait prob"+wc);
		}       
	}

	// returns the element locator and reduces code redundancy
	public By elementLocator(String locator, String locatorType){
		switch (locatorType) 
		{
		case "xpath":
			return By.xpath(locator);
		case "id":
			return By.id(locator);
		case "name":
			return By.name(locator);
		case "link":
			return By.linkText(locator);
		default:
			return null;
		}          
	}

	public void runLocal(String browser){
		
		System.out.println(browser);

		switch (browser) 
		{
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\harshitahluwalia\\Desktop\\HA\\Eclipse\\NRS\\chromedriver.exe");
			driver = new ChromeDriver();
			break;
		case "IE":
			System.setProperty("webdriver.ie.driver", "C:\\Users\\harshitahluwalia\\Desktop\\HA\\Eclipse\\NRS\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			break;
		case "FireFox":
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\harshitahluwalia\\Desktop\\HA\\Eclipse\\NRS\\geckodriver.exe");
			driver = new FirefoxDriver();
			break;
		default:
			System.out.println("No driver for Local");
			break;
		}          
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(mw, TimeUnit.SECONDS);
	}

	//Close all Browsers
	public void closeBrowsers() {
		driver.close();
		driver.quit();
	}

	//generate date
	public static String date() {
		DateFormat dateFormat = new SimpleDateFormat("YY-MM-dd__HH-mm-ss__"); //("MM/dd/yyyy HH:mm:ss")17-11-16 12:08:43
		Date date = new Date();
		String d = dateFormat.format(date);
		return(d);
	}

}
